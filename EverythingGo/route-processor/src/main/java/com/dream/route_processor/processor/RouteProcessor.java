package com.dream.route_processor.processor;


import static javax.lang.model.element.Modifier.PUBLIC;

import com.dream.anno.Route;
import com.dream.route_processor.base.BaseProcessor;
import com.google.auto.service.AutoService;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;

@SupportedSourceVersion(SourceVersion.RELEASE_8)
@AutoService(Processor.class)
public class RouteProcessor extends BaseProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        ClassName listName = ClassName.get(Map.class);
        if (!set.isEmpty()) {
            Set<? extends Element> routes = roundEnvironment.getElementsAnnotatedWith(Route.class);
            try {
                for (Element el : routes) {
                    if (el instanceof VariableElement) {
//                        entityHandler.printNormalMsg("type-name =  " + ((DeclaredType) el.asType()).getTypeArguments().size() + "  end");
//                        List<? extends TypeMirror> args = ((DeclaredType) el.asType()).getTypeArguments();
//                        if (args.size() > 0) {
//                            TypeMirror mirror = args.get(0);
//                            entityHandler.printNormalMsg(">>>  " + ClassName.get(mirror).toString() + "    <<<");
//                            if (ClassName.get((TypeElement) typeUtils.asElement(mirror)).equals(listName)) {
//                                String name1 = ClassName.get(mirror).toString();
//                                String name2 = ClassName.get(elementUtils.getTypeElement("java.util.Map").asType()).toString();
//                                String name3 = ClassName.get((TypeElement) typeUtils.asElement(mirror)).toString();
//                                String name4 = mirror.toString();
//                                entityHandler.printNormalMsg("name1 =  " + name1);
//                                entityHandler.printNormalMsg("name2 =  " + name2);
//                                entityHandler.printNormalMsg("name3 =  " + name3);
//                                entityHandler.printNormalMsg("name4 =  " + name4);
////                                boolean istrue = typeUtils.isSameType(mirror, elementUtils.getTypeElement("java.util.Map").asType());
//                            }
//                        }

                        entityHandler.printNormalMsg("type =  " + el.asType().toString());
                        if (typeUtils.isSameType(el.asType(),elementUtils.getTypeElement("java.lang.String").asType())){
                            entityHandler.printNormalMsg("string par =  " + el.getSimpleName());
                        }
                        TypeName typeName = TypeName.get(el.asType());
                        entityHandler.printNormalMsg("typename  =  " + typeName);
                    }
                }

                JavaFile.builder("com.dream.xrouter.routes",
                        TypeSpec.classBuilder("Wonderful")
                                .addModifiers(PUBLIC)
                                .build()
                ).build().writeTo(mFiler);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }

    @Override
    protected Class<? extends Annotation>[] getSupportedAnnotations() {
        Class[] annos = new Class[]{
                Route.class
        };
        return annos;
    }
}
