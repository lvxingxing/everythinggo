package com.dream.route_processor.base.bean;

public interface Entity {
    String getClassSimpleName();

    String getClassQualifiedName();
}
