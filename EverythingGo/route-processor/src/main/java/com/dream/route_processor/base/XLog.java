package com.dream.route_processor.base;


public class XLog {
    private static Boolean isDebug = true;

    public static void setDebug(Boolean isDebug) {
        XLog.isDebug = isDebug;
    }

    public static Boolean getDebug() {
        return isDebug;
    }
}
