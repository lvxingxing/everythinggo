package com.dream.everythinggo.model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dream.everythinggo.repo.BookRepo
import com.dream.everythinggo.state.BookViewState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch


class BookViewModel : ViewModel() {
    private val _state = MutableStateFlow(BookViewState())
    private val bookRepo = BookRepo

    val state: StateFlow<BookViewState>
        get() = _state

    init {
        viewModelScope.launch {
            _state.emit(BookViewState(true, bookRepo.getBookList()))
        }
    }

    fun onAction(uiAction: UiAction) {
        when (uiAction) {
            is UiAction.RefreshAction -> {
                refreshList()
            }
        }
    }

    sealed class UiAction {
        object RefreshAction : UiAction()
    }


    fun refreshList() {
        viewModelScope.launch {
            _state.emit(BookViewState(true, bookRepo.getRefreshList()))
        }
    }

}


