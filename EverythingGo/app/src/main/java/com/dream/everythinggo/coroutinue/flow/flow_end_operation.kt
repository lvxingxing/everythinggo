package com.dream.everythinggo.coroutinue.flow

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlin.system.measureTimeMillis


suspend fun main() {
    coroutineScope {
//        collected_op()
//        collectIndex_op()
//        collectLastted()
//        launchIn()
        first()
    }
}


/**
 * collected 操作符
 */
suspend fun collected_op() {
    val s = flow {
        for (i in 1..3) {
            println("exe")
            emit(i.toString())
        }
    }
    s.collect {
        println(it)
    }
}

/**
 * 带下标的collect，下标是Flow中的emit顺序。
 */
suspend fun collectIndex_op() {
    val time = measureTimeMillis {
        flow {
            for (i in 1..3) {
                emit(i)
            }
        }.collectIndexed { index, value ->
            run {
                println("$index--$value")
            }
        }
    }
    println(time.toString())
}

/**
 * collectLatest用于在collect中取消未来得及处理的数据，只保留当前最新的生产数据。
 */
suspend fun collectLastted() {
    flow {
        for (i in 1..288800){
            emit(i)
        }
    }.collectLatest {
        delay(1)
        println(it.toString())
    }
}

/**
 * 在指定的协程作用域中直接执行Flow。
 */
suspend fun launchIn(){
    var context = Job() + Dispatchers.IO + CoroutineName("aa")
    flow {
        for (i in 1..4){
            emit(i)
            println(i.toString())
        }
    }.launchIn(CoroutineScope(context))
}

/**
 * 返回第一个
 */
suspend fun first(){
    val a = flow {
        for (i in 20..100){
            emit(i)
        }
    }.first()
    println(a.toString())
}




















