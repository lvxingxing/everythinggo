package com.dream.everythinggo.kotlin_demo

import kotlin.properties.Delegates


fun main() {
//    println(par1)
//    println(par1)

//    println(par2)
//    par2 = "changeValue"
//    println(par2)

//    println(par3)
//    par3 = 50
//    println(par3)
//    par3 = 200
//    println(par3)

    val prot = ProT()
    prot.oldName = "100"
    println(prot.newName)

}

val par1 by lazy {
    println("execu")
    "hahah"
}


var par2 by Delegates.observable("initValue"){
    prop,old,new ->
    println("prop name is ${prop.name}")
    println("oldValue is $old")
    println("newValue is $new")
}

var par3 by Delegates.vetoable(20){
    prop,old,new ->
    if (new > 100){
        println("vote is deny new is $new")
         return@vetoable false
    }
    println("vote pass value is $new")
    return@vetoable true
}

//============================代理给另外一个属性

val topLevel: String = "top prop"
class PropDemo(otherClazz : OtherClazz) {
    //使用top的par代理本属性
    val needProxyPar: String by ::topLevel

    //使用本类中的par代理本属性
    val needAgainProxyPar: String by this::needAgainProxyPar

    //使用其他类的属性代理本属性
    val needProxyByOtheraPar : String by otherClazz::otherProxyPar
}

class OtherClazz(val otherProxyPar : String)


//代理的实际使用场景

class ProT {
    lateinit var newName : String
    @Deprecated("Use newName")
    var oldName : String by this::newName
}













