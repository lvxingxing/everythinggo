package com.dream.everythinggo.kotlin_demo

import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock


inline fun <T> lock(lock: Lock, body: () -> T): T {
    lock.lock()
    try {
        return body()
    } finally {
        lock.unlock()
    }
}

fun <T> get(body: () -> T): T {
    return body()
}

fun main() {
    val l = ReentrantLock()
    lock(l){
        return
    }
}