package com.dream.everythinggo.compose

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.dream.everythinggo.R
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch


class ComposeExampleActivity : AppCompatActivity() {
    var flow = MutableStateFlow("hahaha")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val list = mutableListOf<String>()
        for (i in 1..20) {
            list.add("jksaskjsankasnankasndkandakndkasjdnaskjdnaskdnaskdnakdnaskd = $i")
        }

        setContent {
            MaterialTheme {
//                Conversation(messages = list)
                val name: String by flow.collectAsState()
                HomeScreen (name){
                    lifecycleScope.launch {
                        flow.emit(it)
                    }
                }
            }
        }
    }

    @Composable
    fun HomeScreen(nameValue: String, callback: (String) -> Unit) {
        Column(modifier = Modifier.padding(16.dp)) {
            Text(
                text = "hello",
                modifier = Modifier.padding(8.dp),
                style = MaterialTheme.typography.h5
            )
            OutlinedTextField(
                value = nameValue,
                onValueChange = callback,
                label = { Text(text = "Name") })
        }
    }


    @Composable
    fun Greeting() {
        Column(modifier = Modifier.padding(9.dp)) {
            Image(
                painter = painterResource(id = R.drawable.ic_launcher_background),
                contentDescription = "content",
                modifier = Modifier
                    .width(10.dp)
                    .height(10.dp)
                    .clip(CircleShape)
                    .border(1.2.dp, MaterialTheme.colors.error, CircleShape)
            )
            Text("compose test111")
        }
    }

    @Preview
    @Composable
    fun GreetingPreview() {
        Greeting()
    }

    @Composable
    fun Conversation(messages: List<String>) {
        LazyColumn {
            items(messages) { message ->
                MessageCard(message)
            }
        }
    }

    @Preview
    @Composable
    fun PreviewConversation() {
        val list = mutableListOf<String>()
        for (i in 1..20) {
            list.add("valueksadksnadsandkasndkandkandkasndkandkandaksjdnaskdnaskjdnaskjdnasjkdnaskjdnaskj = $i")
        }
        Conversation(list)
    }

    @Composable
    fun MessageCard(msg: String) {
        Row(modifier = Modifier.padding(all = 8.dp)) {
            Image(
                painter = painterResource(R.drawable.ic_launcher_background),
                contentDescription = null,
                modifier = Modifier
                    .size(40.dp)
                    .clip(CircleShape)
                    .border(1.5.dp, MaterialTheme.colors.secondaryVariant, CircleShape)
            )
            Spacer(modifier = Modifier.width(8.dp))

            // We keep track if the message is expanded or not in this
            // variable
            var isExpanded by remember { mutableStateOf(false) }

            val surfaceColor: Color by animateColorAsState(
                if (isExpanded) MaterialTheme.colors.primary else MaterialTheme.colors.surface,
            )

            // We toggle the isExpanded variable when we click on this Column
            Column(modifier = Modifier.clickable { isExpanded = !isExpanded }) {
                Text(
                    text = msg,
                    color = MaterialTheme.colors.secondaryVariant,
                    style = MaterialTheme.typography.subtitle2
                )

                Spacer(modifier = Modifier.height(4.dp))

                Surface(
                    shape = MaterialTheme.shapes.medium,
                    elevation = 1.dp,
                    color = surfaceColor,
                    // animateContentSize will change the Surface size gradually
                    modifier = Modifier
                        .animateContentSize()
                        .padding(1.dp)
                ) {
                    Text(
                        text = msg,
                        modifier = Modifier.padding(all = 4.dp),
                        // If the message is expanded, we display all its content
                        // otherwise we only display the first line
                        maxLines = if (isExpanded) Int.MAX_VALUE else 1,
                        style = MaterialTheme.typography.body2
                    )
                }
            }
        }
    }
}