package com.dream.everythinggo.coroutinue.flow

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow

fun main() {
    testEmit()
}

fun testEmit() = runBlocking {
    val sharedFlow = MutableSharedFlow<String>(replay = 3)

    val job = launch(Dispatchers.IO) {
        for (i in 1..10) {
            sharedFlow.emit("emit : $i")
            delay(50)
        }
    }


    val readOnlySharedFlow = sharedFlow.asSharedFlow()
    val scope = CoroutineScope(SupervisorJob())
    delay(100)

    val job1 = scope.launch(Dispatchers.IO) {
        readOnlySharedFlow.collect {
            println("receiver1 value = $it")
        }
    }

    delay(200)
    val job2 = scope.launch(Dispatchers.IO) {
        readOnlySharedFlow.collect {
            println("receiver2 value = $it")
        }
    }

    delay(1000)
    job1.cancel()
    job2.cancel()
}