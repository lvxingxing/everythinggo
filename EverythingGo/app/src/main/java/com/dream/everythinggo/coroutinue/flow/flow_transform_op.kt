package com.dream.everythinggo.coroutinue.flow

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*


suspend fun main() {
//    map_op()
//    mapLatest_op()
//    mapNotNull()
//    transform_op()
//    transformLatest_op()
    transformWhile_op()
}

suspend fun map_op() {
    flow {
        for (i in 1..4) {
            emit(i)
        }
    }
        .map { it * it }
        .collect {
            println("$it")
        }
}

/**
 * 在处理的时候只会处理最新的
 */
suspend fun mapLatest_op() {
    flow {
        for (i in 1..3) {
            delay(100)
            emit(i)
        }
    }.mapLatest { value ->
        println("Started computing $value")
        delay(200)
        "Computed $value"
    }.collect {
        println("$it")
    }
}

/**
 * 返回不为null的元素
 */
suspend fun mapNotNull() {
    val list = listOf(1, null, 2, 3)
    flow {
        for (i in list) {
            emit(i)
        }
    }.mapNotNull {
        it
    }.collect {
        println("$it")
    }
}

/**
 * 变换操作，更精细，可以不发，多发等
 */
suspend fun transform_op() {
    flow {
        for (i in 1..4) {
            emit(i)
        }
    }.transform { value ->
        if (value > 2) {
            emit("emit--$value")
        }
    }.collect {
        println(it)
    }
}

/**
 * 处理返回最新的值
 */
suspend fun transformLatest_op() {
    flow {
        for (i in 1..4) {
            delay(100)
            emit(i)
        }
    }.transformLatest { value ->
        println("transform value is $value")
        delay(200)
        emit("final emit value is $value")
    }.collect {
        println(it)
    }
}


/**
 * 控制流的截断
 */
suspend fun transformWhile_op() {
    flow {
        for (i in 1..6) {
            emit(i)
        }
    }.transformWhile { value ->
        if (value > 2) {
            return@transformWhile false
        }
        emit(value)
        return@transformWhile true
    }.collect {
        println(
            "$it"
        )
    }
}








