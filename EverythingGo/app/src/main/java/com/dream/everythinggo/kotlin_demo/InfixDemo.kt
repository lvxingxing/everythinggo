package com.dream.everythinggo.kotlin_demo

fun main() {
    val tt = TT()
    tt cout "asd"
}

infix fun <T> Collection<T>.has(e: T) = contains(e)


class TT {
    infix fun cout(e: String) = println("i am $e")
}

class OtherC{
    fun testHas(){
        val list = listOf("1","2")
        list has ""
    }
}

