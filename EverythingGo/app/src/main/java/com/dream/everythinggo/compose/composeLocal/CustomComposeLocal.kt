package com.dream.everythinggo.compose.composeLocal

import androidx.compose.runtime.compositionLocalOf

data class LocalTest(val weight: Int = 1,val height: Int = 2)

val LocalTestValue = compositionLocalOf { LocalTest() }