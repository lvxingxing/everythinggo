package com.dream.everythinggo.coroutinue.flow

import kotlinx.coroutines.flow.*

suspend fun main() {
    ops()
}

/**
 * 末端操作符在调用之后，创建Flow的代码才会执行
 */
suspend fun ops() {
    flow {
        for (i in 1..10) {
            emit(i)
        }
    }
        .onStart {
            println("start")
        }
            //在发生异常的时候进行重试
        .retryWhen { cause, attempt ->
            true
        }
        .onEach {
            println("value is $it")
        }
        .onCompletion {
            println("complete")
        }
        .catch {

        }
        .collect {
            println(it.toString())
        }
}