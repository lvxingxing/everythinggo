package com.dream.everythinggo.coroutinue.flow

import kotlinx.coroutines.flow.*

suspend fun main() {
//    filter_op()
//    filterInstance()
//    filterNot_op()
//    filterNotNull()
//    drop_op()
//    dropWhile_op()
//    take_op()
//    takeWhile_op()
//    debounce_op()
//    sample_op()
    distinct_op()
}

suspend fun filter_op() {
    flow {
        for (i in 1..5) {
            emit(i)
        }
    }.filter { value ->
        value > 2
    }.collect {
        println("$it")
    }
}

suspend fun filterInstance() {
    val list = listOf(1, 2, null, 3)
    flow {
        for (i in list) {
            emit(i)
        }
    }.filterIsInstance<Int>().collect {
        println("$it")
    }
}

suspend fun filterNot_op() {
    flow {
        for (i in 1..6) {
            emit(i)
        }
    }.filterNot { value -> value > 3 }.collect {
        println("$it")
    }
}

suspend fun filterNotNull() {
    val list = listOf(1, 2, null, 4)
    flow {
        for (i in list) {
            emit(i)
        }
    }.filterNotNull().collect {
        println("$it")
    }
}

/**
 * 丢弃前n个数据
 */
suspend fun drop_op() {
    flow {
        for (i in 1..6) {
            emit(i)
        }
    }.drop(2).collect {
        println("$it")
    }
}

/**
 * 返回整个流
 * 除非第一个就不匹配，整个都不返回了
 */
suspend fun dropWhile_op() {
    flow {
        for (i in 1..6) {
            emit(i)
        }
    }.dropWhile { value -> value > 3 }.collect {
        println("$it")
    }
}

suspend fun take_op() {
    flow {
        for (i in 1..6) {
            emit(i)
        }
    }.take(3).collect {
        println("$it")
    }
}

/**
 * 同理，
 * 只有第一个满足的时候，
 * 整个流才会返回
 */
suspend fun takeWhile_op() {
    flow {
        for (i in 3..7) {
            emit(i)
        }
    }.takeWhile { value -> value > 2 }.collect {
        println("$it")
    }
}

suspend fun debounce_op() {
    flow {
        for (i in 1..10) {
            kotlinx.coroutines.delay(10)
            println("emit")
            emit(i)
        }
    }.debounce(20).collect {
        println("$it")
    }
}

suspend fun sample_op() {
    flow {
        for (i in 1..6) {
            kotlinx.coroutines.delay(10)
            println("emit")
            emit(i)
        }
    }.sample(20).collect {
        println("$it")
    }
}

/**
 * 去重
 */
suspend fun distinct_op() {
    class Inner(val id: Int, val name: String)

    val list = listOf(Inner(1, "111"), Inner(1, "222"), Inner(3, "333"))
    flow {
        for (i in list) {
            emit(i)
        }
    }.distinctUntilChanged { old, new -> old.id == new.id }
        .collect { println(it.name) }
}



