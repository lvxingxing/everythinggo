package com.dream.everythinggo.compose.composeLocal

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.platform.LocalContext

class ComposeLocalExampleActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            MaterialTheme {
                Column {
                    Example()
                    CompositionLocalProvider(
                        LocalContext provides applicationContext,
                        LocalTestValue provides LocalTest(3, 4)
                    ) {
                        Example()
                    }
                }

            }
        }
    }

    @Composable
    fun Example() {
        Column {
            Text("${LocalContext.current}")
            Text("${LocalTestValue.current.height}")
        }
    }
}