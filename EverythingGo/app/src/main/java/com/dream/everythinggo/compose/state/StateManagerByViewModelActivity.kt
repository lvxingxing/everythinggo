package com.dream.everythinggo.compose.state

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.lifecycle.*
import androidx.lifecycle.viewmodel.compose.viewModel


class StateManagerByViewModelActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                ExampleScreen()
            }
        }
    }

    data class ExampleUiState(
        var loading: Boolean = false
    )

    class ExampleViewModel : ViewModel() {
        var uiState by mutableStateOf(ExampleUiState(true))

        fun somethingRelatedToBusinessLogic() {
            uiState = ExampleUiState(false)
        }
    }

    @Composable
    fun ExampleScreen(viewModelee: ExampleViewModel = viewModel()) {
        val uiState = viewModelee.uiState

        Column {
            Text(text = "loadstate = ${uiState.loading}")
            Button(onClick = { viewModelee.somethingRelatedToBusinessLogic() }) {
                Text("Do something")
            }
        }
    }

}