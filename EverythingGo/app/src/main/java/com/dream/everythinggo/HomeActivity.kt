package com.dream.everythinggo

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.dream.everythinggo.coroutinue.flow.TestFlowActivity
import com.dream.everythinggo.flutter.FlutterFragmentActivity
import com.dream.everythinggo.flutter.FlutterMainActivity


class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<TextView>(R.id.tv_gotoflutter).setOnClickListener {
//            toFlutterMainActivity()
            toFlutterFragmentActivity()
        }
    }

    private fun toFlutterMainActivity() {
        FlutterMainActivity.startCurrentActivity(this, "home")
    }

    private fun toFlutterFragmentActivity() {
        val intent = Intent(this, FlutterFragmentActivity::class.java)
        startActivity(intent)
    }
}