package com.dream.everythinggo.compose.state

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.*

class StateManagerByContainer : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                val myAppState = rememberMyAppState()
                Column {
                    Text(text = myAppState.bottomBarTabs)
                    Button(onClick = { myAppState.chanegValue("newvalue") }) {
                        Text(text = "press me")
                    }
                }
            }
        }
    }

    class MyAppState {
        var bottomBarTabs by mutableStateOf("asd")
            private set

        fun chanegValue(va: String) {
            bottomBarTabs = va
        }
    }


    @Composable
    fun rememberMyAppState(
    ) = remember {
        MyAppState()
    }


}