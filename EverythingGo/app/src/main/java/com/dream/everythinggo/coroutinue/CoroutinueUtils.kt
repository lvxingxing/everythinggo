package com.dream.everythinggo.coroutinue

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

suspend fun main() {
    println("main start")
    fetchDocs()
    println("main end")
}

suspend fun fetchDocs() {
    println("start")
    val result = get("https://developer.android.com")
    println("end")
}

suspend fun get(url: String) = withContext(Dispatchers.IO) {
    delay(1000)
    println("get doc...")
}