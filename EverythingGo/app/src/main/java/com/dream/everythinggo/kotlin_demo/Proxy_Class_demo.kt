package com.dream.everythinggo.kotlin_demo

interface Base {
    fun printMessage()
}

class BaseImpl : Base{
    override fun printMessage() {
        println("this is base")
    }
}
class ProxyClass(b : Base) : Base by b{

}

fun main() {
    val impl = BaseImpl()
    ProxyClass(impl).printMessage()
}

