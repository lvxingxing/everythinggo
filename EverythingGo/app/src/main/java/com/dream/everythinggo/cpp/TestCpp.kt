package com.dream.everythinggo.cpp



class TestCpp {
    external fun stringFromJNI(): String?

    companion object {
        /*
         * this is used to load the 'hello-jni' library on application
         * startup. The library has already been unpacked into
         * /data/data/com.example.hellojni/lib/libhello-jni.so
         * at the installation time by the package manager.
         */
        init {
            System.loadLibrary("hello-jni")
        }
    }

    fun invokeNativeMethod(){
        val ss = stringFromJNI()
    }
}
