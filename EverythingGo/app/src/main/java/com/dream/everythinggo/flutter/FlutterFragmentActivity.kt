package com.dream.everythinggo.flutter

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.dream.everythinggo.R
import io.flutter.embedding.android.FlutterFragment
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.embedding.engine.FlutterEngineCache
import io.flutter.embedding.engine.dart.DartExecutor
import io.flutter.plugin.common.BasicMessageChannel
import io.flutter.plugin.common.StringCodec


class FlutterFragmentActivity : FragmentActivity() {
    companion object {
        private const val TAG_FLUTTER_FRAGMENT = "flutter_fragment"
    }

    private var flutterFragment: FlutterFragment? = null
    private var flutterEngine: FlutterEngine? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.flutter_fragment_layout)

        findViewById<TextView>(R.id.tv_send_data).setOnClickListener {
            if (flutterEngine != null){
                flutterEngine?.let {
                    val stringCodecChannel =
                        BasicMessageChannel(it.dartExecutor, "flutterReceiver", StringCodec.INSTANCE)
                    stringCodecChannel.send("这是Android主动发送的数据")
                }
            }else{
                println("null+++++++++++++++++++++")
            }

        }

        val fragmentManager: FragmentManager = supportFragmentManager

        flutterFragment = fragmentManager
            .findFragmentByTag(TAG_FLUTTER_FRAGMENT) as FlutterFragment?

        if (flutterFragment == null) {
            // Instantiate a FlutterEngine.
            flutterEngine = FlutterEngine(this)
            // Configure an initial route.
            flutterEngine?.navigationChannel?.setInitialRoute("basicpage");
            // Start executing Dart code to pre-warm the FlutterEngine.
            flutterEngine?.dartExecutor?.executeDartEntrypoint(
                DartExecutor.DartEntrypoint.createDefault()
            )
            // Cache the FlutterEngine to be used by FlutterActivity or FlutterFragment.
            FlutterEngineCache
                .getInstance()
                .put("firstengine", flutterEngine)
        }


        val flutterFragment: FlutterFragment =
            FlutterFragment.withCachedEngine("firstengine").build()


        this.flutterFragment = flutterFragment
        fragmentManager
            .beginTransaction()
            .add(
                R.id.fragment_container,
                flutterFragment,
                TAG_FLUTTER_FRAGMENT
            )
            .commit()
    }


    override fun onPostResume() {
        super.onPostResume()
        flutterFragment!!.onPostResume()
    }

    override fun onNewIntent(@NonNull intent: Intent) {
        super.onNewIntent(intent)
        flutterFragment!!.onNewIntent(intent)
    }

    override fun onBackPressed() {
        flutterFragment!!.onBackPressed()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        flutterFragment!!.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults
        )
    }

    override fun onUserLeaveHint() {
        flutterFragment!!.onUserLeaveHint()
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        flutterFragment!!.onTrimMemory(level)
    }
}
