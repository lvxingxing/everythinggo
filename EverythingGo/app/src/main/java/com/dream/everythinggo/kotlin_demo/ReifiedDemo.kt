package com.dream.everythinggo.kotlin_demo

import java.lang.reflect.Array.newInstance


interface TypeInterface {
    fun me()
}

inline fun <reified T : TypeInterface> isInstanceOf(value: Any): Boolean {
    return value is T
}

class TypeImpl : TypeInterface {
    override fun me() {
        println("me...")
    }
}

inline fun <reified T> getModel(): T {
    return T::class.java.newInstance()
}


fun skk(aa: TypeImpl = getModel()) {
    aa.me()
}

fun main() {
    println(isInstanceOf<TypeImpl>(value = "222"))
    skk()
}

