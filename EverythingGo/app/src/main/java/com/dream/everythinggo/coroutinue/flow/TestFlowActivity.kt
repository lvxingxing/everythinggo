package com.dream.everythinggo.coroutinue.flow

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class TestFlowActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //            startActivity(Intent(this,ComposeExampleActivity::class.java))
//            startActivity(Intent(this, StateManagerByViewModelActivity::class.java))
//            startActivity(Intent(this,StateManagerByContainer::class.java))
//            startActivity(Intent(this,LifeCycleActivity::class.java))
//            startActivity(Intent(this,LaunchedEffectActivity::class.java))
//            startActivity(Intent(this,SideEffectActivity::class.java))
//            startActivity(Intent(this,ComposeLocalExampleActivity::class.java))
//            startActivity(Intent(this, BookViewActivity::class.java))


        //
//        //绑定声明周期，可以自动取消
//        lifecycleScope.launch {
//            repeatOnLifecycle(Lifecycle.State.STARTED) {
//                readOnlyFlow.collect {
//                    println("$it")
//                }
//            }
//        }
//
//        //绑定hook
//        readOnlyFlow.flowWithLifecycle(lifecycle,Lifecycle.State.STARTED)
//            .onEach {
//
//            }.launchIn(lifecycleScope)
        lifecycleScope.launch(Dispatchers.IO) {
            flow {
                println("flow thread = ${Thread.currentThread().name}")
                emit(6)
            }.map {
                println("map thread = ${Thread.currentThread().name}")
                it * it
            }.flowOn(Dispatchers.IO).map {
                println("map2 thread = ${Thread.currentThread().name}")
                it - 2
            }.flowOn(Dispatchers.Main)
                .collect {
                    println("collect $it ${Thread.currentThread().name}")
                }
        }
    }

}