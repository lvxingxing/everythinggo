package com.dream.everythinggo.coroutinue.flow

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow


fun main() {
    test()
}
val stateFlow = MutableStateFlow(1)
val readOnlyFlow = stateFlow.asStateFlow()
fun test() = runBlocking {
    val jobReceive0 = launch {
        readOnlyFlow.collect {
            println("collect0  value = $it")
        }
    }

    delay(50)
    launch {
        for(i in 1..3){
            println("wait emit $i")
            stateFlow.emit(i)
            delay(50)
        }
    }

    delay(200)

    val jobReceive1 = launch {
        withContext(Dispatchers.IO){
            readOnlyFlow.collect {
                println("collect1 value = $it in thread ${Thread.currentThread().name}")
            }
        }
    }

    val jobReceive2 = launch {
        readOnlyFlow.collect {
            println("collect2 value = $it in thread ${Thread.currentThread().name}")
        }
    }

    delay(200)
    jobReceive0.cancel()
    jobReceive1.cancel()
    jobReceive2.cancel()

}