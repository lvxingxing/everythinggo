package com.dream.everythinggo.state

import com.dream.everythinggo.data.BookEntity

data class BookViewState(
    val showHeader: Boolean = false,
    val bookEntity: BookEntity? = null
)