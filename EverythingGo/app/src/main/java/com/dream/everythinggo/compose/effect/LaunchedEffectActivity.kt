package com.dream.everythinggo.compose.effect

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner

class LaunchedEffectActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                LaunchEffectCompose(state = Movie())
            }
        }
    }

    class Movie {
        val hasError: Boolean = true
    }


    /**
     * 如果LaunchedEffect在recompose时其key不变，那LaunchedEffect不会被重新启动；如果其key发生了变化，则LaunchedEffect会被重新启动。
     */
    @Composable
    fun LaunchEffectCompose(
        state: Movie,
        scaffoldState: ScaffoldState = rememberScaffoldState()
    ) {

        // If the UI state contains an error, show snackbar
        if (state.hasError) {

            // `LaunchedEffect` will cancel and re-launch if
            // `scaffoldState.snackbarHostState` changes
            LaunchedEffect(scaffoldState.snackbarHostState) {
                // Show snackbar using a coroutine, when the coroutine is cancelled the
                // snackbar will automatically dismiss. This coroutine will cancel whenever
                // `state.hasError` is false, and only start when `state.hasError` is true
                // (due to the above if-check), or if `scaffoldState.snackbarHostState` changes.
                scaffoldState.snackbarHostState.showSnackbar(
                    message = "Error message",
                    actionLabel = "Retry message"
                )
            }
        }

        Scaffold(scaffoldState = scaffoldState) {
            /* ... */
        }
    }

}