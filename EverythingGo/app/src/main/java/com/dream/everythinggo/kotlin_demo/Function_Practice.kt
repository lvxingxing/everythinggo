package com.dream.everythinggo.kotlin_demo


//声明一个方法类型
val test1: (a: String, b: String) -> Int = { a, b ->
    0
}

fun testHighOrder(func: (a: String, b: String) -> Int) {
    println("ss")
}

class IntTransformer : (Int) -> Int {
     override fun invoke(ssss: Int): Int = "sss".toInt(ssss)
}

val intFunction: (Int) -> Int = IntTransformer()


fun main() {
    println(test1("1", "2"))


    //匿名fun类型，直接传入
    testHighOrder(fun(a: String, b: String): Int {
        return a.toIntOrNull()?.plus(b.toInt())!!
    })

    testHighOrder(test1)

}