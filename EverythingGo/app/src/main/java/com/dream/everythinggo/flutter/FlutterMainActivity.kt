package com.dream.everythinggo.flutter
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorManager
import android.util.Log
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.android.FlutterActivityLaunchConfigs.BackgroundMode
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.BasicMessageChannel
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.StandardMessageCodec
import java.io.InputStream

class FlutterMainActivity : FlutterActivity() {
    private val CHANNEL = "samples.flutter.dev/battery"
    companion object {
        fun startCurrentActivity(context: Context, initRoute: String) {
            val EXTRA_INITIAL_ROUTE = "route"
            val EXTRA_BACKGROUND_MODE = "background_mode"
            val EXTRA_CACHED_ENGINE_ID = "cached_engine_id"
            val EXTRA_DESTROY_ENGINE_WITH_ACTIVITY = "destroy_engine_with_activity"
            val DEFAULT_BACKGROUND_MODE = BackgroundMode.opaque.name
            val intent =  Intent(context, FlutterMainActivity::class.java)
                .putExtra(EXTRA_INITIAL_ROUTE, initRoute)
                .putExtra(EXTRA_BACKGROUND_MODE, DEFAULT_BACKGROUND_MODE)
                .putExtra(EXTRA_DESTROY_ENGINE_WITH_ACTIVITY, true)
            context.startActivity(intent)
        }
    }

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        flutterEngine
            .platformViewsController
            .registry
            .registerViewFactory("hybrid-view-type", NativeViewFactory())

        //methodchannel
        MethodChannel(flutterEngine.dartExecutor, CHANNEL).setMethodCallHandler {
                call, result ->
            if (call.method == "getNumber") {
                Log.e("xxx", "configureFlutterEngine: ")
                result.success(1024)
            }
        }

        //eventchannel
        val sensorManger: SensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val accelerometerSensor: Sensor = sensorManger.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        EventChannel(flutterEngine.dartExecutor, "eventChannelDemo")
            .setStreamHandler(AccelerometerStreamHandler(sensorManger, accelerometerSensor))

        //basicmessagechannel
        BasicMessageChannel(flutterEngine.dartExecutor, "platformImageDemo", StandardMessageCodec())
            .setMessageHandler { message, reply ->
                if (message == "getImage") {
                    val inputStream: InputStream = assets.open("huoying.jpeg")
                    reply.reply(inputStream.readBytes())
                }
            }

    }
}