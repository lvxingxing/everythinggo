package com.dream.everythinggo.coroutinue.flow

import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withTimeout

suspend fun main() {
    cancel()
}

/**
 * 取消本质是依赖协程的
 */
suspend fun cancel(){
    withTimeout(2500){
        flow {
            for (i in 1..6){
                kotlinx.coroutines.delay(1000)
                emit(i)
            }
        }.collect {
            println("$it")
        }
    }
}