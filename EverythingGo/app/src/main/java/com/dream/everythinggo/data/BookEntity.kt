package com.dream.everythinggo.data

data class BookEntity(
    val list:List<BookItem>,
)

data class BookItem(
    val name: String,
    val price: Int
)
