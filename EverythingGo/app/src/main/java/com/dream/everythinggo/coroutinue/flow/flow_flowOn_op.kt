package com.dream.everythinggo.coroutinue.flow

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
fun main() {
//    flowOn_op()
    runBlocking {
        launch(Dispatchers.IO) {
            flow {
                println("flow thread = ${Thread.currentThread().name}")//main
                emit(6)
            }.map {
                println("map thread = ${Thread.currentThread().name}")//main
                it * it
            }.flowOn(Dispatchers.Main).map {
                println("map2 thread = ${Thread.currentThread().name}")//io
                it - 2
            }.flowOn(Dispatchers.IO)
                .collect {
                    println("collect $it ${Thread.currentThread().name}")//io
                }
        }
    }
}
