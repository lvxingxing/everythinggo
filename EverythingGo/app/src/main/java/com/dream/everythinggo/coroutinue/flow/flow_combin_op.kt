package com.dream.everythinggo.coroutinue.flow

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*

suspend fun main() {
//    combine_op()
//    merge_op()
//    zip_op()
}

//suspend fun combine_op() {
//    val flow1 = flowOf(1,2,3).onEach { delay(20) }
//
//    val flow2 = flowOf("a","b","c","d").onEach { delay(20) }
//
//    flow1.combine(flow2) { value1, value2 ->
//        value1.toString() + value2
//    }.collect {
//        println(it)
//    }
//}
//
///**
// * merge之后是无序的
// */
//suspend fun merge_op(){
//    val flow1 = flowOf(1,2,3).onEach { delay(10) }
//
//    val flow2 = flowOf("a","b","c","d").onEach { delay(20) }
//
//    listOf(flow1,flow2).merge().collect {
//        println("$it")
//    }
//}
//
//suspend fun zip_op(){
//    val flow1 = flowOf(1,2,3).onEach { delay(10) }
//
//    val flow2 = flowOf("a","b","c","d").onEach { delay(20) }
//
//    flow1.zip(flow2) { i, s -> i.toString() + s }.collect {
//        println("$it")
//    }
//}