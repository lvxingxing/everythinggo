package com.dream.everythinggo.repo

import com.dream.everythinggo.data.BookEntity
import com.dream.everythinggo.data.BookItem


object BookRepo {

    fun getBookList(): BookEntity {
        val list = mutableListOf<BookItem>()
        for (i in 1..20) {
            list.add(BookItem("name$i", i * 10))
        }
        return BookEntity(list)
    }

    fun getRefreshList(): BookEntity {
        val list = mutableListOf<BookItem>()
        for (i in 1..5) {
            list.add(BookItem("refresh$i", i * 5))
        }
        return BookEntity(list)
    }
}