package com.dream.everythinggo.compose.effect

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.material.*
import androidx.compose.runtime.*

class RememberUpdateComposeActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                rememberUpdateStateCompose()
            }
        }
    }

    @Composable
    fun rememberUpdateStateCompose() {
        val snackbarHostState = remember { SnackbarHostState() }
        var text by remember { mutableStateOf("") }
        var descrption by remember { mutableStateOf("") }

        LaunchedEffectWrapper1(snackbarHostState, text) { descrption = it }

        // .. code for handling the change of the text state
        Scaffold(
//     attach snackbar host state to the scaffold
            scaffoldState = rememberScaffoldState(snackbarHostState = snackbarHostState),
            content = {
                Column(
                    content = {
                        OutlinedTextField(
                            value = text,
                            onValueChange = {
                                text = it
                            },
                            label = { Text("Input") }
                        )
                        Text(descrption)
                    }
                )
            }
        )
    }

    /**
     * LaunchedEffect在key不变的情况下不会重启，如果其中有依赖的某个值想使用最新的值，又不想重启，就需要使用rememberUpdatedState
     */
    @Composable
    fun LaunchedEffectWrapper1(state: SnackbarHostState, text: String, onResult: (String) -> Unit) {
        val mostRecentText by rememberUpdatedState(text)
        //使用Unit参数，创建与调用点的生命周期匹配的effect
        LaunchedEffect(Unit) {
            val snackbarResult = state.showSnackbar(
                message = "Are you happy with your input?",
                actionLabel = "Yes",
                duration = SnackbarDuration.Indefinite,
            )
            // Do something with text parameter
            when (snackbarResult) {
                SnackbarResult.ActionPerformed -> {
                    onResult("final text:$mostRecentText")
                }
                else -> {}
            }
        }
    }
}