package com.dream.everythinggo.coroutinue.flow

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.channelFlow

fun main() {
    testChannelFlow()
}

fun testChannelFlow() = runBlocking {
    val flow = channelFlow<String> {
        send("11")
        println("send first on ${Thread.currentThread().name}")
        withContext(Dispatchers.IO) {
            send("22")
            println("send second on ${Thread.currentThread().name}")
        }
        send("33")
        println("send third on ${Thread.currentThread().name}")
        awaitClose {
            println("awaitClose")
        }
    }

    val job = launch {
        flow.collect {
            println("collect result on ${Thread.currentThread().name}")
            println("result $it")
        }
    }

    delay(200)
    job.cancel()
}