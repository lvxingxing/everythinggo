package com.dream.everythinggo.view

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.dream.everythinggo.data.BookItem
import com.dream.everythinggo.model.BookViewModel

class BookViewActivity : AppCompatActivity() {
    private val viewModel: BookViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            MaterialTheme {
                Column {
                    Button(onClick = { viewModel.onAction(BookViewModel.UiAction.RefreshAction) }) {
                        Text("press to refresh")
                    }
                    BookList()
                }
            }
        }
    }

    @Composable
    fun BookList() {
        val uiState = viewModel.state.collectAsState()
        LazyColumn {
            items(uiState.value.bookEntity!!.list) { item ->
                BookCard(item = item)
            }
        }
    }

    @Composable
    fun BookCard(item: BookItem) {
        Column(modifier = Modifier.padding(10.dp)) {
            Text(item.name)
            Text("price:${item.price}")
        }
    }
}