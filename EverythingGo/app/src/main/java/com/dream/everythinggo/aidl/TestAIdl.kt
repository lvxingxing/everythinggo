package com.dream.everythinggo.aidl

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder

fun testAidl(con: Context) {
    var iRemoteService: IMyAidlInterface? = null

    val mConnection = object : ServiceConnection {

        // Called when the connection with the service is established
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            // Following the example above for an AIDL interface,
            // this gets an instance of the IRemoteInterface, which we can use to call on the service
            iRemoteService = IMyAidlInterface.Stub.asInterface(service)
            iRemoteService?.printAidlMessage("hehe?++")
        }

        // Called when the connection with the service disconnects unexpectedly
        override fun onServiceDisconnected(className: ComponentName) {
            iRemoteService = null
        }
    }

    val intent = Intent(con, RemoteService::class.java)
    con.bindService(intent, mConnection, Context.BIND_AUTO_CREATE)
}