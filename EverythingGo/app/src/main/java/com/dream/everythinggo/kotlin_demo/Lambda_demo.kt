package com.dream.everythinggo.kotlin_demo

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.viewModel

fun main() {
//    test {
//        toString()
//    }
//    test2 {
//        it.toString()
//    }
//
////    val sss = sumOb(6,5)
////    println("$sss")
//
//
//    val person = Person("xx", 1)
//    with(person) {
//        println("my name is ${name}, ${age} years old.")
//    }
//
//    mytest(2) {
//        println("sssss")
//        return@mytest 2
//    }
//
    val a = "ttt".toIntOrNull() ?: "not number"
    println(a)
}

fun mytest(par1: Int, par2: () -> Int) {
    println("$par1")
    par2.invoke()
}

data class Person(var name: String, var age: Int)

fun with(par2: Person, block: Person.() -> Unit) {
    par2.block()
}


fun test(function: Int.() -> String): String {
    return 5.function()
}

fun test2(function: (Int) -> String): String {
    return function(5)
}

val sumOb: Int.() -> Int = { this.plus(6) }

abstract class A
class B : ViewModel() {

}












