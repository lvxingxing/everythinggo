package com.dream.everythinggo.compose.effect

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*


class SideEffectActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                Example()
            }
        }
    }

    @Composable
    fun Example() {
        println("recompose")
        var user1 by remember {
            mutableStateOf(User())
        }

        val anaRes: Analytics = rememberAnalytics(user1)

        Column {
            Text(text = anaRes.anaName)
            Button(onClick = {
                println("onclick")
                val u = User()
                u.changeName("222")
                user1 = u
            }) {
                Text(text = "Press me")
            }
        }
    }

    class User {
        var userName: String = "111"

        fun changeName(value: String) {
            userName = value
        }
    }

    class Analytics {
        var anaName: String = "000"

        fun changeName(na: String) {
            anaName = na
        }
    }


    @Composable
    fun rememberAnalytics(user: User): Analytics {
        println("rememberAnalytics recompose")
        val ana: Analytics = remember {
            Analytics()
        }

        SideEffect {
            println("side-exe ${user.userName}")
            ana.changeName(user.userName)
        }
        return ana
    }

}