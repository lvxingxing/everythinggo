package com.dream.everythinggo.coroutinue

import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.selects.select


fun main() {
    testSelect()
}

fun testSelect() = runBlocking {
    val d1 = async {
        delay(100)
        1
    }
    val d2 = async {
        delay(200)
        2
    }

    val d3 = async {
        delay(300)
        3
    }

    val ddd = select<Int> {
        d3.onAwait {
                data ->
            println("d3 exe")
            data
        }
        d1.onAwait {
            data ->
            println("d1 exe")
            data
        }

        d2.onAwait {
                data ->
            println("d2 exe")
            data
        }

    }
    println("result $ddd")
}