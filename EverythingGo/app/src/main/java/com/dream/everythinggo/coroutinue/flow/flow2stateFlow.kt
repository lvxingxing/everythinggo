package com.dream.everythinggo.coroutinue.flow

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.shareIn


fun main() {
    test1()
}

fun test1() = runBlocking {
    val scope = CoroutineScope(SupervisorJob())
    val hotFlow = flow {
        withContext(Dispatchers.IO){
            for (i in 1..9){
                emit(i)
            }
        }
    }.shareIn(scope, SharingStarted.Eagerly)



    val job = launch {
        hotFlow.collect {
            println("$it")
        }
    }

}