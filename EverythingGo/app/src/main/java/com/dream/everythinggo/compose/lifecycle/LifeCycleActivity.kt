package com.dream.everythinggo.compose.lifecycle

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable

class LifeCycleActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme {
                val list = mutableListOf<MyItem>()
                for (i in 1..10) {
                    list.add(MyItem("name$i==", i))
                }
                MyColumn(list)
            }
        }
    }

    data class MyItem(val name: String, val id: Int)

    @Composable
    fun MyColumn(data: List<MyItem>) {
        LazyColumn {
            //指定key唯一标识,避免顺序变化导致的重组
            items(data, key = { item -> item.id }) {
                Text(text = it.name)
            }
        }
    }

    // Marking the type as stable to favor skipping and smart recompositions.
    //使用此注解可以声明一个稳定的类型
    @Stable
    interface UiState<T : Result<T>> {
        val value: T?
        val exception: Throwable?

        val hasError: Boolean
            get() = exception != null
    }

}