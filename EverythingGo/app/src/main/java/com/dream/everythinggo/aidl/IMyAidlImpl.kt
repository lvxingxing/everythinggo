package com.dream.everythinggo.aidl

val binder = object : IMyAidlInterface.Stub() {
    override fun basicTypes(
        anInt: Int,
        aLong: Long,
        aBoolean: Boolean,
        aFloat: Float,
        aDouble: Double,
        aString: String?
    ) {
        TODO("Not yet implemented")
    }

    override fun printAidlMessage(str: String?) {
        println("this is remote message execute$str")
    }

}