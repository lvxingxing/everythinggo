
import 'package:flutter/material.dart';

class NativeFragmentPage extends StatefulWidget{
  const NativeFragmentPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() =>  NativeFragmentPageState();

}

class NativeFragmentPageState extends State<NativeFragmentPage>{
  @override
  Widget build(BuildContext context) {
      return const Material(
        color: Colors.green,
        child: SizedBox(
          height: 200,
          width: double.infinity,
          child: Center(
            child: Material(
              color: Colors.deepOrange,
              child: Text("这是flutter =fragment"),
            ),
          ),
        )
      );
  }

}