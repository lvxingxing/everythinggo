import 'dart:math';
import 'package:flutter/material.dart';

class StickyPage extends StatelessWidget {
  const StickyPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        _buildStickyBar(),
        _buildStickyBar1(),
        _buildList(),
      ],
    );
  }

  Widget _buildStickyBar() {
    return SliverAppBar(
      automaticallyImplyLeading: false,
      expandedHeight: 200.0,
      floating: true,
      stretch: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          collapseMode: CollapseMode.pin,
          title: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: const [
                Text(
                  "搜索栏",
                  style: TextStyle(backgroundColor: Colors.deepOrange),
                )
              ],
            ),
          ),
          background: Image.asset('assets/images/mao2.jpeg')),
    );
  }

  Widget _buildStickyBar1() {
    return SliverPersistentHeader(
      pinned: true, //是否固定在顶部
      floating: true,
      delegate: _SliverAppBarDelegate(
          minHeight: 100, //收起的高度
          maxHeight: 100, //展开的最大高度
          child: Container(
            margin: const EdgeInsets.only(top: 50),
            padding: const EdgeInsets.only(left: 16),
            color: Colors.pink,
            alignment: Alignment.centerLeft,
            child: const Text("产品推荐", style: TextStyle(fontSize: 18)),
          )),
    );
  }

  Widget _buildList() {
    return SliverList(
        delegate: SliverChildBuilderDelegate(
      (context, index) {
        return Container(
          height: 50,
          color: index % 2 == 0 ? Colors.white : Colors.black12,
          width: double.infinity,
          alignment: Alignment.center,
          child: Text("我是第$index个item"),
        );
      },
      childCount: 30,
    ));
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate({
    required this.minHeight,
    required this.maxHeight,
    required this.child,
  });

  final double minHeight;
  final double maxHeight;
  final Widget child;

  @override
  double get minExtent => minHeight;

  @override
  double get maxExtent => max(maxHeight, minHeight);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight ||
        child != oldDelegate.child;
  }
}
