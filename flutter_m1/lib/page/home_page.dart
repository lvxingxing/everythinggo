import 'package:flutter/material.dart';
import 'package:flutter_m1/channel/demo_page/basic_message_demo_page.dart';
import 'package:flutter_m1/channel/demo_page/channel_demo_page.dart';
import 'package:flutter_m1/page/appbar_page.dart';
import 'package:flutter_m1/page/hero_page.dart';
import 'package:flutter_m1/page/main_page.dart';
import 'package:flutter_m1/page/scrollbar_page.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
        create: (context) => InfoModel(),
        child: const FirstPage(),
      );
}

class FirstPage extends StatefulWidget {
  const FirstPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController;
  late Animation anim;

  int _selectedIndex = 0;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    MainPage(),
    MainPage1(),
    StickyPage(),
    BasicMessageChannelPage()
  ];
  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 1000));
    anim = Tween(begin: 50, end: 200).animate(animationController)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          Fluttertoast.showToast(msg: "finish anim");
        }
      });
  }

  void _onItemTapped(int currentIndex) {
    setState(() {
      _selectedIndex = currentIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        showUnselectedLabels: true,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: '发现',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: '已接单',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            label: '已发布',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            label: '我的',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        unselectedItemColor: Colors.grey[500],
        onTap: _onItemTapped,
      ),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text('主页'),
      ),
      body: Container(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
    );
  }
}

class InfoModel with ChangeNotifier {
  int _count = 0;
  late String _name;
  late String _name2;

  String get name2 => _name2;

  set name2(String value) {
    add();
    _name2 = value + _count.toString();
    notifyListeners();
  }

  String get name => _name;

  set name(String value) {
    add();
    _name = value + _count.toString();
    notifyListeners();
  }

  void add() {
    _count++;
  }
}
