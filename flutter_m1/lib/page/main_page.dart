import 'package:flutter/material.dart';
import 'package:flutter_m1/page/hero_page.dart';
import 'package:flutter_m1/page/home_page.dart';
import 'package:flutter_m1/route/CommonRoute.dart';
import 'package:provider/provider.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MainPageState();
  }
}

class MainPageState extends State<MainPage> {
  final List<String> entries = <String>['A'];
  final List<int> colorCodes = <int>[600];

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < 20; i++) {
      entries.add("iitem $i");
      colorCodes.add(600);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<InfoModel>(
      builder: (context, model, _) => Stack(
        alignment: Alignment.bottomRight,
        children: [
          DefaultTabController(
              length: entries.length,
              child: Scaffold(
                body: NestedScrollView(
                  headerSliverBuilder: (context, bool) {
                    return [
                      SliverAppBar(
                        automaticallyImplyLeading: false,
                        expandedHeight: 100.0,
                        floating: false,
                        pinned: true,
                        flexibleSpace: FlexibleSpaceBar(
                          stretchModes: const [StretchMode.fadeTitle],
                          centerTitle: true,
                          title: SizedBox(
                              child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    alignment: Alignment.center,
                                    child: const Text("招聘22"),
                                    decoration: BoxDecoration(
                                        color: Colors.cyan,
                                        borderRadius:
                                            BorderRadius.circular(3.0)),
                                    height: 30.0,
                                  ),
                                  Expanded(
                                    child: Container(
                                      alignment: Alignment.center,
                                      child: const Text("面试"),
                                      decoration: BoxDecoration(
                                          color: Colors.greenAccent,
                                          borderRadius:
                                              BorderRadius.circular(3.0)),
                                      height: 30.0,
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      alignment: Alignment.center,
                                      child: const Text("交友"),
                                      decoration: BoxDecoration(
                                          color: Colors.green,
                                          borderRadius:
                                              BorderRadius.circular(3.0)),
                                      height: 30.0,
                                    ),
                                  )
                                ],
                                mainAxisSize: MainAxisSize.max,
                              ),
                            ],
                          )),
                        ),
                      ),
                    ];
                  },
                  body: ListView.builder(
                      padding: const EdgeInsets.all(14.0),
                      itemCount: entries.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                          margin: const EdgeInsets.all(0.5),
                          color: Colors.amber[colorCodes[index]],
                          child: SizedBox(
                            child: Center(
                              child: Text(
                                'Entry ${entries[index]}',
                              ),
                            ),
                            height: 60.0,
                          ),
                        );
                      }),
                ),
              )),
          Padding(
              padding: const EdgeInsets.all(20.0),
              child: Hero(
                tag: "publish",
                child: ElevatedButton(
                  onPressed: () {
                    CommonRoute.toPublish(context);
                  },
                  child: const Text("发布"),
                ),
              ))
        ],
      ),
    );
  }
}
