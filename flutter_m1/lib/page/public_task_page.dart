import 'package:flutter/material.dart';

class PublishTaskPage extends StatefulWidget {
  const PublishTaskPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => PublishTaskPageState();
}

class PublishTaskPageState extends State<PublishTaskPage> {
  @override
  Widget build(BuildContext context) {
    TextEditingController _controller = TextEditingController();

    return Scaffold(
      // appBar: AppBar(title: Text('NewPage')),
      body: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: GestureDetector(
                  child: Hero(
                      tag: 'publish',
                      child: Image.asset('assets/images/mao2.jpeg')),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ],
          ),
          TextField(
            decoration: const InputDecoration(
                labelText: "请输入任务名称",
                hintText: "请输入用户名",
                prefixIcon: Icon(Icons.people_alt_rounded)),
            controller: _controller,
          ),

          TextField(
            decoration: const InputDecoration(
                labelText: "请输入任务介绍",
                hintText: "请输入用户名",
                prefixIcon: Icon(Icons.people_alt_rounded)),
            controller: _controller,
          ),

          TextField(
            decoration: const InputDecoration(
                labelText: "请输入任务悬赏",
                hintText: "请输入用户名",
                prefixIcon: Icon(Icons.people_alt_rounded)),
            controller: _controller,
          ),

          TextField(
            decoration: const InputDecoration(
                labelText: "请输入任务期限",
                hintText: "请输入用户名",
                prefixIcon: Icon(Icons.people_alt_rounded)),
            controller: _controller,
          ),
        ],
      ),
    );
  }
}
