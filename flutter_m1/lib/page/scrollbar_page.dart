import 'package:flutter/material.dart';

const url =
    'http://www.pptbz.com/pptpic/UploadFiles_6909/201203/2012031220134655.jpg';

class MainPage1 extends StatefulWidget {
  const MainPage1({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MainPage1State();
  }
}

class MainPage1State extends State<MainPage1> {
  var tabTitle = [
    '页面1',
    '页面2',
    '页面3',
  ];

  final List<String> entries = <String>['A'];
  final List<int> colorCodes = <int>[600];

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < 20; i++) {
      entries.add("value $i");
      colorCodes.add(600);
    }
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollView(
        headerSliverBuilder: (context, bool) {
          return [
            SliverAppBar(
              automaticallyImplyLeading: false,
              expandedHeight: 200.0,
              floating: true,
              stretch: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  collapseMode: CollapseMode.pin,
                  title: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: const [Text("111")],
                    ),
                  ),
                  background: Image.asset('assets/images/mao2.jpeg')),
            ),
          ];
        },
        body: ListView.builder(
            padding: const EdgeInsets.all(14.0),
            itemCount: entries.length,
            itemBuilder: (BuildContext context, int index) {
              return Card(
                margin: const EdgeInsets.all(0.5),
                color: Colors.amber[colorCodes[index]],
                child: SizedBox(
                  child: Center(
                    child: Text(
                      'Entry ${entries[index]}',
                    ),
                  ),
                  height: 60.0,
                ),
              );
            }));
  }
}

class SliverTabBarDelegate extends SliverPersistentHeaderDelegate {
  final TabBar widget;
  final Color color;

  const SliverTabBarDelegate(this.widget, {required this.color});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      child: widget,
      color: color,
    );
  }

  @override
  bool shouldRebuild(SliverTabBarDelegate oldDelegate) {
    return false;
  }

  @override
  double get maxExtent => widget.preferredSize.height;

  @override
  double get minExtent => widget.preferredSize.height;
}
