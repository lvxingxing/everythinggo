import 'package:flutter/material.dart';
import 'package:flutter_m1/platform_view/android/native_view_example.dart';
import 'package:flutter_m1/route/CommonRoute.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Column(
          children: [
            Center(
              child: SizedBox(
                width: 300,
                height: 300,
                child: Center(
                  child: buildView(context),
                ),
              ),
            )
          ],
        ),
        onTap: () {
          CommonRoute.toSecond(context);
        });
  }
}
