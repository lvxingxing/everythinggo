import 'package:flutter/material.dart';

class SecondPage extends StatefulWidget {
  const SecondPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SecondPageState();
  }
}

class SecondPageState extends State<SecondPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: const Text("主轴排列"),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              //主轴排列
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                //使用expanded防止过度绘制问题
                const Expanded(
                  child: Image(image: AssetImage('assets/images/mao2.jpeg')),
                  flex: 2,
                ),
                Expanded(
                  child: Image.asset('assets/images/mao2.jpeg'),
                  flex: 3,
                ),
                Expanded(
                  child: Image.asset('assets/images/mao3.jpeg'),
                  flex: 2,
                )
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.star, color: Colors.green[500]),
                Icon(Icons.star, color: Colors.green[500]),
                Icon(Icons.star, color: Colors.green[500]),
                const Icon(Icons.star, color: Colors.black),
                const Icon(Icons.star, color: Colors.black),
              ],
            ),
            Stack(
              alignment: const Alignment(0, 0),
              children: [
                const CircleAvatar(
                  backgroundImage: AssetImage('assets/images/mao1.jpeg'),
                  radius: 100,
                ),
                Container(
                  decoration: const BoxDecoration(
                    color: Colors.black45,
                  ),
                  child: const Text(
                    'Stack Word',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 210,
              width: 300,
              child: Card(
                elevation: 24,
                child: Column(
                  children: [
                    ListTile(
                      title: const Text(
                        '1625 Main Street',
                        style: TextStyle(fontWeight: FontWeight.w500),
                      ),
                      subtitle: const Text('My City, CA 99984'),
                      leading: Icon(
                        Icons.restaurant_menu,
                        color: Colors.blue[500],
                      ),
                    ),
                    const Divider(),
                    ListTile(
                      title: const Text(
                        '(408) 555-1212',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          color: Color(0xffff00ff),
                          fontFamily: "Courier",
                        ),
                      ),
                      leading: Icon(
                        Icons.contact_phone,
                        color: Colors.blue[500],
                      ),
                    ),
                    ListTile(
                      title: const Text.rich(TextSpan(children: [
                        TextSpan(text: "111111"),
                        TextSpan(
                          text: "https://flutterchina.club",
                          style: TextStyle(color: Colors.blue),
                        ),
                        TextSpan(
                            text: '2222',
                            style: TextStyle(color: Colors.deepOrange)),
                      ])),
                      leading: Icon(
                        Icons.contact_mail,
                        color: Colors.blue[500],
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ));
  }
}
