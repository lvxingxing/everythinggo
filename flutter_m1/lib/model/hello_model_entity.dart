import 'dart:convert';
import 'package:flutter_m1/generated/json/base/json_field.dart';
import 'package:flutter_m1/generated/json/hello_model_entity.g.dart';

@JsonSerializable()
class HelloModelEntity {

	String? name;
	String? keywords;
	int? age;
  
  HelloModelEntity();

  factory HelloModelEntity.fromJson(Map<String, dynamic> json) => $HelloModelEntityFromJson(json);

  Map<String, dynamic> toJson() => $HelloModelEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}