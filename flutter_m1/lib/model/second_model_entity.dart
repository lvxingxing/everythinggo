import 'dart:convert';
import 'package:flutter_m1/generated/json/base/json_field.dart';
import 'package:flutter_m1/generated/json/second_model_entity.g.dart';

@JsonSerializable()
class SecondModelEntity {

	String? name;
	String? keywords;
	int? age;
  
  SecondModelEntity();

  factory SecondModelEntity.fromJson(Map<String, dynamic> json) => $SecondModelEntityFromJson(json);

  Map<String, dynamic> toJson() => $SecondModelEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}