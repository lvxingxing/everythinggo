import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_m1/channel/demo_page/basic_message_channel_double_page.dart';
import 'package:flutter_m1/page/home_page.dart';
import 'package:flutter_m1/page/native_fragment_page.dart';
import 'package:flutter_m1/page/splash_screen.dart';

void main() => runApp(MyApp(window.defaultRouteName));

class MyApp extends StatelessWidget {
  String route = "splash";

  MyApp(this.route, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<HomePageState> detalisKey = GlobalKey();

    switch (route) {
      case "home":
        return MaterialApp(
          title: 'Flutter百科',
          theme: ThemeData(
              primaryColor: Colors.greenAccent,
              iconTheme: const IconThemeData(color: Colors.yellow),
              textTheme: const TextTheme(button: TextStyle(color: Colors.red))),
          home: const HomePage(),
        );

      case "basicpage":
        return MaterialApp(
          title: 'Flutter百科',
          theme: ThemeData(
              primaryColor: Colors.greenAccent,
              iconTheme: const IconThemeData(color: Colors.yellow),
              textTheme: const TextTheme(button: TextStyle(color: Colors.red))),
          home: const BasicMessageChannelDoublePage(),
        );

      default:
        return MaterialApp(
          title: '大学堂',
          theme: ThemeData(
              primaryColor: Colors.greenAccent,
              iconTheme: const IconThemeData(color: Colors.yellow),
              textTheme: const TextTheme(button: TextStyle(color: Colors.red))),
          home: Scaffold(
            appBar: AppBar(
              // Here we take the value from the MyHomePage object that was created by
              // the App.build method, and use it to set our appbar title.
              title: const Text("导航栏default"),
            ),
            body: const NativeFragmentPage(),
          ),
        );
    }
  }
}
