import 'package:flutter/material.dart';
import 'package:flutter_m1/page/home_page.dart';
import 'package:flutter_m1/page/public_task_page.dart';
import 'package:flutter_m1/page/second_page.dart';

class CommonRoute {
  static void toHome(
    context,
  ) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return const HomePage();
    }));
  }

  static void toSecond(context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return const HomePage();
    }));
  }

  static void toPublish(context) {
    Navigator.push(context, PageRouteBuilder(pageBuilder: (BuildContext context,
        Animation<double> animatin, Animation<double> second) {
      return FadeTransition(opacity: animatin, child: const PublishTaskPage());
    }));
  }
}
