import 'package:flutter/services.dart';

class Accelerometer {
  static const _eventChannel = EventChannel('eventChannelDemo');

  /// Method responsible for providing a stream of [AccelerometerReadings] to listen
  /// to value changes from the Accelerometer sensor.
  static Stream<AccelerometerReadings> get readings {
    return _eventChannel.receiveBroadcastStream().map(
          (dynamic event) => AccelerometerReadings(
        event[0] as double,
        event[1] as double,
        event[2] as double,
      ),
    );
  }
}

class AccelerometerReadings {
  /// Acceleration force along the x-axis.
  final double x;

  /// Acceleration force along the y-axis.
  final double y;

  /// Acceleration force along the z-axis.
  final double z;

  AccelerometerReadings(this.x, this.y, this.z);
}