import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BasicMessageChannelDoublePage extends StatefulWidget {
  const BasicMessageChannelDoublePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => BasicMessageChannelDoublePageState();
}

class BasicMessageChannelDoublePageState
    extends State<BasicMessageChannelDoublePage> {
  var receiveName = "default";

  @override
  void initState() {
    super.initState();
    const BasicMessageChannel<String?>("flutterReceiver", StringCodec())
        .setMessageHandler((message) async {
      if (message == null) {
        setState(() {
          receiveName = "没有发送过来正常数据";
        });
      } else {
        setState(() {
          receiveName = message;
        });
      }
      return "no data";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(child: Text("这是数据$receiveName"));
  }
}
