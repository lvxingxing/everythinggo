import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_m1/channel/basic_message_channel.dart';

class BasicMessageChannelPage extends StatefulWidget {
  const BasicMessageChannelPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => BasicMessageChannelPageState();

}

class BasicMessageChannelPageState extends State<BasicMessageChannelPage> {
  Future<Uint8List>? imageData;

  @override
  void initState() {
    super.initState();
    setState(() {
      imageData = PlatformImageFetcher.getImage();
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Uint8List>(future: imageData,builder: (context,snapshot){
      if(snapshot.connectionState == ConnectionState.none){
        return const Placeholder();
      }else if(snapshot.hasError){
        return Center(
          child: Text(
            (snapshot.error as PlatformException).message!,
          ),
        );
      } else if(snapshot.connectionState == ConnectionState.done){
        return Image.memory(
          snapshot.data!,
          fit: BoxFit.fill,
        );
      }
      return const CircularProgressIndicator();
    },);
  }

}