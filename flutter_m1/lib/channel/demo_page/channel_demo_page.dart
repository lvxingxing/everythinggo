import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_m1/channel/event_channel.dart';

class EventChannelPage extends StatefulWidget {
  const EventChannelPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => EventChannelPageState();
}

class EventChannelPageState extends State<EventChannelPage> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<AccelerometerReadings>(
      stream: Accelerometer.readings,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Text((snapshot.error as PlatformException).message!);
        } else {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'x axis: ' + snapshot.data!.x.toStringAsFixed(3),
              ),
              Text(
                'y axis: ' + snapshot.data!.y.toStringAsFixed(3),
              ),
              Text(
                'z axis: ' + snapshot.data!.z.toStringAsFixed(3),
              )
            ],
          );
        }
      },
    );
  }
}
