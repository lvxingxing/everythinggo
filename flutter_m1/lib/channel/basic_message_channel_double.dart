import 'package:flutter/services.dart';

class NativeMessageReceiver {
  static const _nativeMessageReceiver =
      BasicMessageChannel("flutterReceiver", StandardMessageCodec());

  static void receiveNativeMessage() async {
    const BasicMessageChannel<String?>('flutterReceiver', StringCodec())
        .setMessageHandler((message) async {
      if (message == null) {
      } else {

      }
      return null;
    });
  }
}
