import 'package:flutter_m1/generated/json/base/json_convert_content.dart';
import 'package:flutter_m1/model/second_model_entity.dart';

SecondModelEntity $SecondModelEntityFromJson(Map<String, dynamic> json) {
	final SecondModelEntity secondModelEntity = SecondModelEntity();
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		secondModelEntity.name = name;
	}
	final String? keywords = jsonConvert.convert<String>(json['keywords']);
	if (keywords != null) {
		secondModelEntity.keywords = keywords;
	}
	final int? age = jsonConvert.convert<int>(json['age']);
	if (age != null) {
		secondModelEntity.age = age;
	}
	return secondModelEntity;
}

Map<String, dynamic> $SecondModelEntityToJson(SecondModelEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['name'] = entity.name;
	data['keywords'] = entity.keywords;
	data['age'] = entity.age;
	return data;
}