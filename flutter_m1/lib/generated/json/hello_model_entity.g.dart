import 'package:flutter_m1/generated/json/base/json_convert_content.dart';
import 'package:flutter_m1/model/hello_model_entity.dart';

HelloModelEntity $HelloModelEntityFromJson(Map<String, dynamic> json) {
	final HelloModelEntity helloModelEntity = HelloModelEntity();
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		helloModelEntity.name = name;
	}
	final String? keywords = jsonConvert.convert<String>(json['keywords']);
	if (keywords != null) {
		helloModelEntity.keywords = keywords;
	}
	final int? age = jsonConvert.convert<int>(json['age']);
	if (age != null) {
		helloModelEntity.age = age;
	}
	return helloModelEntity;
}

Map<String, dynamic> $HelloModelEntityToJson(HelloModelEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['name'] = entity.name;
	data['keywords'] = entity.keywords;
	data['age'] = entity.age;
	return data;
}