import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


Widget buildView(BuildContext context) {
  // This is used in the platform side to register the view.
  const String viewType = 'hybrid-view-type';
  // Pass parameters to the platform side.
  Map<String, dynamic> creationParams = <String, dynamic>{};
  creationParams["par1"] = "这是原生View";
  creationParams["par2"] = "222";
  creationParams["par3"] = "333";


  return AndroidView(
    viewType: viewType,
    layoutDirection: TextDirection.ltr,
    creationParams: creationParams,
    creationParamsCodec: const StandardMessageCodec(),
  );
}